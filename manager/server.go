package manager

import (
	"dragonfly/common/config"
)

func Run(cfg *config.Config) error {
	if err := InitManager(cfg); err != nil {
		return err
	}

	manager.dialAgent()
	go manager.StartTask()

	select {}

	return nil
}
