package collector

import (
	"dragonfly/common/config"
)

func Run(cfg *config.Config) error {
	if err := InitCollector(cfg); err != nil {
		return err
	}

	if err := InitAgentPool(&collector); err != nil {
		return err
	}

	go collector.StartTask()

	select {}
	return nil
}
