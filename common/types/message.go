package types

import (
	"dragonfly/common/tcp"
)

type MessageType byte

//消息类型定义
const (
	MESS_POST_HOST_CONFIG MessageType = iota
	MESS_POST_METRIC
	MESS_POST_TSD
	MESS_GET_HOST_PLUGIN_LIST
	MESS_GET_HOST_PLUGIN_LIST_RESP
	MESS_GET_ALL_PLUGIN_MD5
	MESS_GET_ALL_PLUGIN_MD5_RESP
	MESS_PULL_PLUGIN
	MESS_POST_HOST_ALIVE
)

const (
	MsgGetHeartbeat tcp.PacketType = iota
	MsgSendHeartbeat
	MsgCollectorGetTimeSeriesData
	MsgAgentSendTimeSeriesData
	MsgCheckAgentPluginsList
)

var MsgTextMap map[tcp.PacketType]string = map[tcp.PacketType]string{
	MsgGetHeartbeat:               "MsgGetHeartbeat",
	MsgSendHeartbeat:              "MsgSendHeartbeat",
	MsgCollectorGetTimeSeriesData: "MsgGetTimeSeriesData",
	MsgAgentSendTimeSeriesData:    "MsgAgentSendTimeSeriesData",
	MsgCheckAgentPluginsList:      "MsgAgentGetPluginsList",
}
