package types

import (
	"dragonfly/common/msgpack"
)

type HeartBeat struct {
	Alive   bool
	Message string
}

func (this *HeartBeat) Encode() []byte {
	data, _ := msgpack.Marshal(this)
	return data
}

func (this *HeartBeat) Decode(data []byte) error {
	return msgpack.Unmarshal(data, &this)
}

func (this *HeartBeat) IsAlive() bool {
	return this.Alive
}
