package types

import (
	"dragonfly/common/msgpack"
)

type Host struct {
	Name string
}

func (this *Host) Encode() []byte {
	data, _ := msgpack.Marshal(this)
	return data
}

func (this *Host) Decode(data []byte) error {
	return msgpack.Unmarshal(data, &this)
}
