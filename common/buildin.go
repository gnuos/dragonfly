package common

//NewBool 返回参数的bool指针
func NewBool(b bool) *bool { return &b }

//NewInt 返回参数的int指针
func NewInt(n int) *int { return &n }

//NewInt64 返回参数的int64指针
func NewInt64(n int64) *int64 { return &n }

//NewString 返回参数的string指针
func NewString(s string) *string { return &s }
