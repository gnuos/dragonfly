package log

import (
	"os"
	"time"

	"dragonfly/common/config"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	jww "github.com/spf13/jwalterweatherman"
)

func NewLogger(s *config.LogSettings) *zerolog.Logger {
	var lg zerolog.Logger

	if s.EnableConsole {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
		return &log.Logger
	} else if s.EnableFile {
		logFile, err := os.OpenFile(*s.FileLocation, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0666)
		if err != nil {
			jww.ERROR.Printf("%T %s \n", time.Now(), "Log file is not found, Writing logs to Stdout")
		}

		lg = zerolog.New(logFile).With().Timestamp().Logger()
		defer logFile.Close()
		return &lg
	} else {
		return &log.Logger
	}
}
