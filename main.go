package main

import (
	"dragonfly/cmd"
	_ "dragonfly/cmd"
	"os"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
