package cmd

import (
	"dragonfly/collector"
	"dragonfly/common/config"

	"github.com/spf13/cobra"
)

var collectorCmd = &cobra.Command{
	Use:   "collector",
	Short: "Start collect metrics",
	RunE:  collectorCmdFn,
}

func init() {
	RootCmd.AddCommand(collectorCmd)
}

func collectorCmdFn(command *cobra.Command, args []string) error {
	configFile, err := command.Flags().GetString("config")
	if err != nil {
		return err
	}

	cfg, err := config.LoadConfig(configFile)
	if err != nil {
		return err
	}

	return collector.Run(cfg)
}
