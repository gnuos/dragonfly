package cmd

import (
	"fmt"
	"os"
)

//CommandPrintln 打印普通文本
func CommandPrintln(a ...interface{}) (int, error) {
	return fmt.Println(a...)
}

//CommandPrintErrorln 打印报错信息
func CommandPrintErrorln(a ...interface{}) (int, error) {
	return fmt.Fprintln(os.Stderr, a...)
}

//CommandPrettyPrintln 打印到标准错误输出
func CommandPrettyPrintln(a ...interface{}) (int, error) {
	return fmt.Fprintln(os.Stderr, a...)
}
