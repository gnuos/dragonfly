package cmd

import (
	"dragonfly/common/config"
	"dragonfly/manager"

	"github.com/spf13/cobra"
)

var (
	managerCmd = &cobra.Command{
		Use:   "manager [command]",
		Short: "Manager can query host status.",
		RunE:  managerCmdFn,
	}

	cliCmd = &cobra.Command{
		Use:   "cli",
		Short: "Use cli to manage agent and collector",
		RunE:  cliCmdFn,
	}
)

func init() {
	managerCmd.AddCommand(
		cliCmd,
	)
	RootCmd.AddCommand(managerCmd)
}

func managerCmdFn(command *cobra.Command, args []string) error {
	configFile, err := command.Flags().GetString("config")
	if err != nil {
		return err
	}

	cfg, err := config.LoadConfig(configFile)
	if err != nil {
		return err

	}

	return manager.Run(cfg)
}

func cliCmdFn(command *cobra.Command, args []string) error {
	//config, err := command.Flags().GetString("config")
	//if err != nil {
	//	return err
	//}

	return nil
}
