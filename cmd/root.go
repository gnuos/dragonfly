package cmd

import (
	"runtime"

	"github.com/spf13/cobra"
	jww "github.com/spf13/jwalterweatherman"
)

var (
	versionFlag bool
	debugFlag   bool
)

var RootCmd = &cobra.Command{
	Use:   "dragonfly [command]",
	Short: "dragonfly allows you manage every agent without open web ui.",
	Long:  "Dragonfly is an open source and distributed monitoring system.",
	RunE: func(c *cobra.Command, args []string) error {
		if versionFlag {
			jww.FEEDBACK.Println("Dragonfly version", Version)
			return nil
		}
		return c.Help()
	},
	BashCompletionFunction: bashCompletionFunc,
}

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	RootCmd.Flags().BoolVarP(&versionFlag, "version", "v", false, "show version")
	RootCmd.Flags().BoolVarP(&debugFlag, "debug", "d", true, "print debug level logs")
	RootCmd.PersistentFlags().StringP("config", "c", "config.json", "Configuration file to use.")
}
