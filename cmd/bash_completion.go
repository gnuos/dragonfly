package cmd

const (
	bashCompletionFunc = `# bash completion for dragonfly
_dragonfly() {
    local cur prev words cword
    _init_completion || return

    local commands command options formats

    commands='agent collector manager pipeline scheduler'

    if [[ $cword -eq 1 ]]; then
        COMPREPLY=( $( compgen -W "$commands" -- "$cur" ) )
    else
        command=${words[1]}
        case "$prev" in
            '-c'|'--config')
                _filedir -d
                ;;
            *)
                COMPREPLY=($(compgen -W "$( _parse_help "$1" -help ) 
                    -d --debug -v --version" -- "$cur"))
                ;;
        esac

        if [[ "$cur" == -* ]]; then
            # possible options for the command
            case $command in
                validate)
                    options='-c --config'
                    ;;
                cli)
                    options='-c --config'
                    ;;
            esac
            COMPREPLY=( $( compgen -W "$options" -- "$cur" ) )
        else
            if [[ "$command" == gen ]]; then
                COMPREPLY=( $( compgen -W 'config autocomplete' -- "$cur" ) )
            else
                _filedir
            fi
        fi
    fi
} &&
complete -F _dragonfly dragonfly

`
)
