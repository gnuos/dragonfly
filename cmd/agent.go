package cmd

import (
	"dragonfly/agent"
	"dragonfly/common/config"

	"github.com/spf13/cobra"
)

var agentCmd = &cobra.Command{
	Use:   "agent",
	Short: "Run agent",
	RunE:  agentCmdFn,
}

func init() {
	RootCmd.AddCommand(agentCmd)
}

func agentCmdFn(command *cobra.Command, args []string) error {
	configFile, err := command.Flags().GetString("config")
	if err != nil {
		return err
	}

	cfg, err := config.LoadConfig(configFile)
	if err != nil {
		return err
	}

	return agent.Run(cfg)
}
