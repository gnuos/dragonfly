package agent

import (
	"dragonfly/common/config"
	_ "net/http/pprof"
)

func Run(cfg *config.Config) error {
	if err := InitAgent(cfg); err != nil {
		return err
	}
	select {}
	return nil
}
