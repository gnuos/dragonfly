package agent

import (
	"dragonfly/common/config"
	"dragonfly/common/log"
	"dragonfly/common/tcp"
	"dragonfly/common/types"

	"github.com/rs/zerolog"
)

var (
	agent *Agent
)

type Agent struct {
	*tcp.AsyncTCPServer
	hostinfo  *types.Host
	settings  *config.AgentSettings
	log       *zerolog.Logger
	collector *tcp.TCPConn
	manager   *tcp.TCPConn
}

func InitAgent(c *config.Config) error {
	protocol := &tcp.DefaultProtocol{}
	protocol.SetMaxPacketSize(0)
	s := tcp.NewAsyncTCPServer(
		*c.AgentSettings.ListenAddress,
		&callback{},
		protocol,
	)

	agent = &Agent{
		s,
		&types.Host{Name: "agent"},
		&c.AgentSettings,
		log.NewLogger(&c.LogSettings),
		&tcp.TCPConn{},
		&tcp.TCPConn{},
	}

	agent.log.Info().Msgf("Listening on %s", *agent.settings.ListenAddress)
	return agent.ListenAndServe()
}
