package agent

import (
	"io/ioutil"
	"runtime"
	"strconv"
	"strings"
)

const (
	uuid_file   = "/sys/devices/virtual/dmi/id/product_uuid"
	uptime_file = "/proc/uptime"
)

func getUptime(args *string) (float64, float64) {
	var (
		uptimeStr string
		idleStr   string
	)
	if data, err := ioutil.ReadFile(uptime_file); err == nil {
		fields := strings.Split(strings.TrimSpace(string(data)), " ")
		if len(fields) == 2 {
			uptimeStr = fields[0]
			idleStr = fields[1]
		}
	}
	up, _ := strconv.ParseFloat(uptimeStr, 64)
	idle, _ := strconv.ParseFloat(idleStr, 64)
	if up != 0 {
		idle = (idle / (up * float64(runtime.NumCPU()))) * 100
	}
	return up, idle
}
